<?php

namespace App\Controller;

use App\Repository\FilmRepository;
use App\Repository\UserRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin/gestion_des_utilisateurs", name="admin_users")
     */
    public function gestionDesUtilisateurs(UserRepository $userRepository)
    {
        $users = $userRepository->findAll();

        return $this->render('admin/gestionDesUtilisateurs.html.twig', [
            'users' => $users,
        ]);
    }

     /**
     * @Route("/admin/gestion_des_films", name="admin_films")
     */
    public function gestionDesFilms(FilmRepository $filmRepository)
    {
        $films = $filmRepository->findAll();

        return $this->render('admin/gestionDesFilms.html.twig', [
            'films' => $films,
        ]);
    }
}
