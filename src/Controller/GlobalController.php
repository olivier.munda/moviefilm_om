<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class GlobalController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function accueil()
    {
        return $this->render('global/accueil.html.twig');
    }

    /**
     * @Route("/inscription", name="registration")
     */
    public function registration()
    {
        return $this->render('global/inscription.html.twig');
    }

    /**
     * @Route("/connexion", name="login")
     */
    public function login()
    {
        return $this->render('global/connexion.html.twig');
    }

    /**
     * @Route("/deconnexion", name="logout")
     */
    public function logout()
    {

    }

    /**
     * @Route("/a_propos", name="a_propos")
     */
    public function aPropos()
    {
        return $this->render('global/a_propos.html.twig');
    }
}
