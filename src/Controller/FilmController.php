<?php

namespace App\Controller;

use App\Repository\FilmRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FilmController extends AbstractController
{
    /**
     * @Route("/films", name="films")
     */
    public function films(FilmRepository $repository)
    {
        $films = $repository->findAll();

        return $this->render('film/films.html.twig', [
            'films' => $films,
        ]);
    }
}
