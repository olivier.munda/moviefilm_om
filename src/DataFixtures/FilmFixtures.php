<?php

namespace App\DataFixtures;

use App\Entity\Film;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class FilmFixtures extends Fixture
{
      public function load(ObjectManager $manager)
      {
            // Film1
            $film1 = new Film();
            $film1->setTitle('Baywatch : Alerte à Malibu')
                  ->setDescription('Le légendaire sauveteur Mitch Buchannon est contraint de s’associer à une nouvelle recrue, Matt Brody, aussi ambitieux que tête brûlée ! Ensemble, ils vont tenter de déjouer un complot criminel qui menace l avenir de la Baie…')
                  ->setDirector('Seth Gordon')
                  ->setActors(['Dayne Johnson', 'Zac efron', 'Alexandra Daddario'])
                  ->setReleaseYear(mt_rand(2017))
                  ->setDuration(mt_rand(119))
                  ->setImage('baywatch.jpeg');
            $manager->persist($film1);

            // Film2
            $film2 = new Film();
            $film2->setTitle('ÇA : CHAPITRE 2')
                  ->setDescription('27 ans après la victoire du Club des Ratés sur Grippe-Sou, le sinistre Clown est de retour pour semer la terreur dans les rues de Derry. Désormais adultes, les membres du Club ont tous quitté la petite ville pour faire leur vie. Cependant, lorsqu on signale de nouvelles disparitions d enfants, Mike, le seul du groupe à être demeuré sur place, demande aux autres de le rejoindre. Traumatisés par leur expérience du passé, ils doivent maîtriser leurs peurs les plus enfouies pour anéantir Grippe-Sou une bonne fois pour toutes. Mais il leur faudra d abord affronter le Clown, devenu plus dangereux que jamais…')
                  ->setDirector('Andy Muschietti')
                  ->setActors(['Bill Skarsgård', 'James McAvoy', 'Jessica Chastain'])
                  ->setReleaseYear(mt_rand(2019))
                  ->setDuration(mt_rand(170))
                  ->setImage('ça_episode2.jpeg');
            $manager->persist($film2);

            // Film3
            $film3 = new Film();
            $film3->setTitle('Star Wars : L Ascension de Skywalker')
                  ->setDescription('Lucasfilm et le réalisateur J.J. Abrams unissent de nouveau leurs forces pour embarquer le spectateur dans un voyage épique vers une galaxie lointaine, très lointaine. Dans ce dernier acte captivant de la prodigieuse saga Skywalker, de nouvelles légendes voient le jour. L ultime combat pour la liberté est à venir. Après le retour mystérieux de Palpatine, la Résistance affronte à nouveau le Premier Ordre. De nouvelles légendes vont naître dans cette bataille épique pour la liberté. La conclusion de la saga Skywalker.')
                  ->setDirector('J.J. Abrams')
                  ->setActors(['Carrie Fisher', 'Mark Hamill', 'Daisy Ridley', 'Adam Driver', 'John Boyega', 'Oscar Isaac'])
                  ->setReleaseYear(mt_rand(2019))
                  ->setDuration(mt_rand(141))
                  ->setImage('l_ascension_de_skywalker.jpeg');
            $manager->persist($film3);

            // Film4
            $film4 = new Film();
            $film4->setTitle('Star Wars : Le Réveil de la Force')
                  ->setDescription('Il y a bien longtemps, dans une galaxie lointaine… Luke Skywalker est porté disparu. Le pilote Poe est en mission secrète sur une planète pour le retrouver. Au moment où la diabolique armée "Premier Ordre" apparaît en détruisant tout sur son passage, il arrive à cacher la position géographique de l ancien maître Jedi dans son droïde BB-8. Capturé par les larbins du machiavélique Kylo Ren, Poe est libéré par le soldat ennemi Finn qui est en pleine crise existentielle. Pendant ce temps, BB-8 est recueillie par Rey, une pilleuse d épaves qui sera bientôt plongée dans une quête qui la dépasse.')
                  ->setDirector('J.J. Abrams')
                  ->setActors(['Carrie Fisher', 'Mark Hamill', 'Daisy Ridley', 'Adam Driver', 'John Boyega', 'Oscar Isaac'])
                  ->setReleaseYear(mt_rand(2015))
                  ->setDuration(mt_rand(135))
                  ->setImage('le reveil de la force.jpeg');
            $manager->persist($film4);

            // Film5
            $film5 = new Film();
            $film5->setTitle('Star Wars : Les Derniers Jedi')
                  ->setDescription('Le Premier Ordre étend ses tentacules aux confins de l univers, poussant la Résistance dans ses retranchements. Il est impossible de se sauver à la vitesse de la lumière avec cet ennemi continuellement aux trousses. Cela n empêche pas Finn et ses camarades de tenter d identifier une brèche chez leur adversaire. Pendant ce temps, Rey se trouve toujours sur la planète Ahch-To pour convaincre Luke Skywalker de lui enseigner les rudiments de la Force.')
                  ->setDirector('Rian Johnson')
                  ->setActors(['Carrie Fisher', 'Mark Hamill', 'Daisy Ridley', 'Adam Driver', 'John Boyega', 'Oscar Isaac'])
                  ->setReleaseYear(mt_rand(2017))
                  ->setDuration(mt_rand(152))
                  ->setImage('les derniers jedi.jpeg');
            $manager->persist($film5);

            // Film6
            $film6 = new Film();
            $film6->setTitle('Titanic')
                  ->setDescription('Southampton, 10 avril 1912. Le paquebot le plus grand et le plus moderne du monde, réputé pour son insubmersibilité, le « Titanic », appareille pour son premier voyage. 4 jours plus tard, il heurte un iceberg. À son bord, un artiste pauvre et une grande bourgeoise tombent amoureux.')
                  ->setDirector('James Cameron')
                  ->setActors(['Leonardo DiCaprio', 'Kate Winslet', 'Billy Zane'])
                  ->setReleaseYear(mt_rand(1997))
                  ->setDuration(mt_rand(194))
                  ->setImage('titanic.jpeg');
            $manager->persist($film6);

            // Film7
            $film7 = new Film();
            $film7->setTitle('Avatar')
                  ->setDescription('Malgré sa paralysie, Jake Sully, un ancien marine immobilisé dans un fauteuil roulant, est resté un combattant au plus profond de son être. Il est recruté pour se rendre à des années-lumière de la Terre, sur Pandora, où de puissants groupes industriels exploitent un minerai rarissime destiné à résoudre la crise énergétique sur Terre. Parce que l atmosphère de Pandora est toxique pour les humains, ceux-ci ont créé le Programme Avatar, qui permet à des "pilotes" humains de lier leur esprit à un avatar, un corps biologique commandé à distance, capable de survivre dans cette atmosphère létale. Ces avatars sont des hybrides créés génétiquement en croisant l ADN humain avec celui des Na vi, les autochtones de Pandora.')
                  ->setDirector('James Cameron')
                  ->setActors(['Sam Worthington', 'Zoe Saldana', 'Stephen Lang', 'Sigourney Weaver', 'Joel David Moore'])
                  ->setReleaseYear(mt_rand(2009))
                  ->setDuration(mt_rand(166))
                  ->setImage('avatar.jpeg');
            $manager->persist($film7);

            // Film8
            $film8 = new Film();
            $film8->setTitle('Les Bronzés 3 : Amis pour la vie')
                  ->setDescription('En 1978, Popeye, Gigi, Jérôme, Bernard, Nathalie et Jean-Claude faisaient connaissance en Côte d Ivoire dans un club de vacances. Amours, coquillages et crustacés. Un an plus tard, retrouvailles du groupe d amis à Val d Isère. Tire-fesses, fartage et pistes verglacées. Après le Club Med et le ski, ils n ont cessé de se voir, de se perdre de vue, de se retrouver, de se reperdre, de se revoir pour des semaines de vacances volées à une vie civile assommante. Depuis quelques années, ils se retrouvent chaque été, pour une semaine, au Prunus Resort, hôtel de luxe et de bord de mer, dont Popeye s occupe plus ou moins bien en tant que gérant, et qui appartient à sa femme, Graziella Lespinasse, héritière d une des plus grosses fortunes italiennes. Que sont devenus les Bronzés 27 ans après ? Réponse hâtive :')
                  ->setDirector('Patrice Leconte')
                  ->setActors(['Josiane Balasko', 'Michel Blanc', 'Marie-Anne Chazel', 'Christian Clavier', 'Gérard Jugnot', 'Thierry Lhermitte'])
                  ->setReleaseYear(mt_rand(2006))
                  ->setDuration(mt_rand(97))
                  ->setImage('les bronzés_3.jpeg');
            $manager->persist($film8);

            // Film9
            $film9 = new Film();
            $film9->setTitle('En eaux troubles')
                  ->setDescription('Missionné par un programme international d observation de la vie sous-marine, un submersible a été attaqué par une créature gigantesque qu on croyait disparue. Sérieusement endommagé, il gît désormais dans une fosse, au plus profond de l océan Pacifique, où son équipage est pris au piège. Il n y a plus de temps à perdre : Jonas Taylor, sauveteur-plongeur expert des fonds marins, est engagé par un océanographe chinois particulièrement visionnaire, contre l avis de sa fille Suyin.')
                  ->setDirector('Jon Turteltaub')
                  ->setActors(['Jason Statham', 'Li Bingbing', 'Rainn Wilson', 'Cliff Curtis', 'Ruby Rose', 'Winston Chao'])
                  ->setReleaseYear(mt_rand(2018))
                  ->setDuration(mt_rand(113))
                  ->setImage('en eaux troubles.jpeg');
            $manager->persist($film9);

            $manager->flush();
      }
}
