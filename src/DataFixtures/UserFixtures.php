<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // User1
        $user1 = new User();
        $user1->setUsername('zitou')
            ->setFirstname('zitoun')
            ->setAge(47)
            ->setPassword('2412Tr@3872')
            ->setRoles(['ROLE_ADMIN']);
        $manager->persist($user1);

        // User2
        $user2 = new User();
        $user2->setUsername('belou')
            ->setFirstname('isabelle')
            ->setAge(42)
            ->setPassword('belou43')
            ->setRoles(['ROLE_USER']);
        $manager->persist($user2);

        // User3
        $user2 = new User();
        $user2->setUsername('Giugiu')
            ->setFirstname('giulia')
            ->setAge(16)
            ->setPassword('mungiu')
            ->setRoles(['ROLE_USER']);
        $manager->persist($user3);

        // User4
        $user2 = new User();
        $user2->setUsername('Gagan')
            ->setFirstname('morgan')
            ->setAge(9)
            ->setPassword('mungan')
            ->setRoles(['ROLE_USER']);
        $manager->persist($user4);

        $manager->flush();
    }
}
